from django.test import TestCase, Client
from django.urls import resolve
from django.contrib.auth import get_user_model

class TestURL(TestCase):

	def test_homepage_url_is_exist(self):
		response = Client().get('')
		self.assertEqual(response.status_code, 200)
		
	def test_using_homepage_template(self):
		response = Client().get('')
		self.assertTemplateUsed(response, 'homepage.html')
	
	def test_signup_url_is_exist(self):
		response = Client().get('/signup/')
		self.assertEqual(response.status_code, 200)
		
	def test_using_signup_template(self):
		response = Client().get('/signup/')
		self.assertTemplateUsed(response, 'registration/signup.html')
	
	def test_signed_up_url_is_exist(self):
		response = Client().get('/signed_up/')
		self.assertEqual(response.status_code, 200)
		
	def test_using_signed_up_template(self):
		response = Client().get('/signed_up/')
		self.assertTemplateUsed(response, 'registration/signed_up.html')
		
	def test_logged_out_url_is_exist(self):
		response = Client().get('/logged_out/')
		self.assertEqual(response.status_code, 200)
		
	def test_using_logged_out_template(self):
		response = Client().get('/logged_out/')
		self.assertTemplateUsed(response, 'registration/logged_out.html')


class TestSignUp(TestCase):
	
	def setUp(self):
		self.username = 'olivia'
		self.password1 = 'asdfghjk'
		self.password2 = 'asdfghjk'
	
	def test_signup_create_new_user(self):
		response = self.client.post('/signup/', data = {
		'username' : self.username,
		'password1' : self.password1,
		'password2' : self.password2,
		})
		
		users = get_user_model().objects.filter(username='olivia')
		#self.assertEqual(users.count(), 1)
		