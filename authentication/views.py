from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate
from django.http import HttpResponseRedirect
from django.contrib.auth.forms import UserCreationForm

def homepage(request):
	current_user = request.user
	name = current_user.get_username()
	return render(request, 'homepage.html', {'name':name})

def signup(request):
	if request.method == 'POST':
		form = UserCreationForm(request.POST)
		if form.is_valid():
			form.save()
			username = form.cleaned_data.get('username')
			raw_password = form.cleaned_data.get('password1')
			user = authenticate(username=username, password=raw_password)
			return HttpResponseRedirect('/signed_up/')
	else:
		form = UserCreationForm()
	return render(request, 'registration/signup.html', {'form':form})
	
def signed_up(request):
	return render(request, 'registration/signed_up.html')

def logged_out(request):
	return render(request, 'registration/logged_out.html')