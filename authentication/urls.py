from django.urls import path, include
from . import views

urlpatterns = [
	path('', views.homepage),
	path('', include('django.contrib.auth.urls')),
	path('logged_out/', views.logged_out),
	path('signup/', views.signup),
	path('signed_up/', views.signed_up),
]